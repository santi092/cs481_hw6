﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Connectivity;
using System.Net.Http;
using Xamarin.Forms;

namespace HW6
{

    public partial class MainPage : ContentPage
    {
        public string Entry { get; set; }
        ObservableCollection<Definition> definitionList;

        public MainPage()
        {
            // Initialize list and sets binding context
            InitializeComponent();
            BindingContext = this;
            definitionList = new ObservableCollection<Definition>();
            x.ItemsSource = definitionList; // sets x source to def list

        }

        async void getWord_clicked(object sender, EventArgs e)
        {
            // if statement checks for internet connection
            if (CrossConnectivity.Current.IsConnected)
            {
                //HttpClient object
                HttpClient client = new HttpClient();

                //HttpRequestMessage
                var uri = new Uri(string.Format
                    ($"https://owlbot.info/api/v2/dictionary/" + $"{Entry}"));


                // http request
                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;

                HttpResponseMessage response = await client.SendAsync(request);

                // Initialize word object 
                Definition[] wordData = null;
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    wordData = Definition.FromJson(content);

                }
                else
                {
                    await DisplayAlert("Error", "Issue retrieving data", "OK");
                }

                // Initialize definiton attributes
                string dtype = "";
                string def = "";
                string ex = "";
                // clears list
                definitionList.Clear();

                //adds definition information to list
                for (int i = 0; i < wordData.Length; i++)
                {
                    dtype = wordData[i].type;
                    def = wordData[i].definition;
                    ex = wordData[i].example;

                    definitionList.Add(new Definition { type = dtype, definition = def, example = ex });
                }

            }
            else
            {
               await DisplayAlert("Notice", "No internet connection", "OK");

            }





        }
    }
}